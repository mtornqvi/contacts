import readFromDatabase from "./connectDB.js";

export default function displayAllContacts(contacts) {
    let outputString = "";
    
    for (let entryCounter in Object.entries(contacts)) {
 
        const entry = contacts[entryCounter];
        outputString = 
        `UUID : ${entry.uuid} \n` +
        `First name : ${entry.firstName} \n` +
        `Last name : ${entry.lastName}  \n` +
        `Email : ${entry.email}  \n` +
        `Phone : ${entry.phone}  \n` +
        `Address : ${entry.address} \n` +  
        `Notes : ${entry.notes} \n`;  

        console.table(outputString);
        outputString = ""; // reset for next entry 
    } 
}

export const displayAll = async () => {
    const data = await readFromDatabase();
    displayAllContacts(data);
};