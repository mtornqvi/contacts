
export const InputValidator = async (contacts) => {
    let findEmpty = Object.values(contacts)
        .filter((value) => value === "");

    let valid = false;
    while (!valid) { 
        if (findEmpty.length >= (Object.values(contacts).length - 3)) {
            valid = false;
            console.log("Atleast one more input is needed.\n");
            return -1; //
        } else {
            valid = true;
            console.log("New Contact added.\n");
        }
    }
};
