import fs from "fs";
import getUserInput from "./getUserInput.js";
import askQuestion from "./asyncQuestion.js";
import {displayAll} from "./displayAllContacts.js";
import { editContact } from "./editContact.js";
import { removeEntry } from "./removeEntry.js";

const main = async () => {

    let answer = "";
    answer = await askQuestion("Do you want to : \n" +
    "1. Add an entry?\n" + 
    "2. Remove an entry?\n" +
    "3. Edit an entry?\n" +
    "4. Listing all contact details?\n" +
    "Q. Quit?\n" +
    "Please input 1-4 or Q\n");

    switch(answer) {
    case "1":
        await getUserInput();
        break;
    case "2":
        await removeEntry();
        break;
    case "3":
        await editContact();
        break;                
    case "4":
        await displayAll();
        break;
    case "Q":
    case "q":
        console.log("Thank you for using the program. Bye!");
        process.exit(1); 
    default:
        console.log("Invalid input. Please try again.");
    }

    main(); // recursively call main function

};
    
main();

