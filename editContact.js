import {readFromDatabase, writeToDatabase} from "./connectDB.js";
import askQuestion from "./asyncQuestion.js";
import displayAllContacts from "./displayAllContacts.js";
import {getFirstName,
    getLastName,
    getPhoneNumber,
    getEmailAddress,
    getAddress,
    getNotes} from "./getUserInput.js";


const template = {
    uuid: "uuidv4",
    firstName: "Teppo",
    lastName: "Testaaja",
    email: "teppo@testaaja.fi",
    phone: "+358400123123",
    address: "Mannerheimintie 1, 00100 Helsinki",
};

/* const writeTemplate = () => {
    writeToDatabase(template)
} */

const question = `
What do you want to change:
1. firstName:
2. lastName
3. email
4. phone
5. Address
6. Notes

Select 'q' to go back to main menu.
`;

/** 
 * A function that searches for a contact based on first and last name.
 * Takes object as a parameter and return the index of the search result. 
 **/
export const findContactIndex = async (database) => {
    let match = false;
    let index = "";
    while (!match) {
        let findFirstName = await askQuestion("Firstname to find: ");
        let findLastName = await askQuestion("Last name to find: ");
        findFirstName = findFirstName.toLowerCase();
        findLastName = findLastName.toLowerCase();
        
        index = await database.findIndex(x => 
            x.firstName.toLowerCase().includes(findFirstName) && 
                    x.lastName.toLowerCase().includes(findLastName)
        );
        if (index !== -1) {
            match = true;
        } else console.log("\nNo match, try again:");
    }
    
    console.log("\nFOUND");
        
    displayAllContacts([database[index]]);
    return index;
}; 

const editEntry =  async (entry) => {
    const data = await readFromDatabase();
    let contactIndex = await findContactIndex(data);
    console.log("MODIFY FOLLOWING DETAILS:");
    switch(entry) {
    case "1":
        data[contactIndex].firstName = await getFirstName();
        break;
    case "2":
        data[contactIndex].lastName = await getLastName();
        break;
    case "3":
        data[contactIndex].email = await getEmailAddress();
        break;
    case "4":
        data[contactIndex].phone = await getPhoneNumber();
        break;
    case "5":
        data[contactIndex].address = await getAddress();
        break;
    case "6":
        data[contactIndex].notes = await getNotes();
        break;
    default:
        console.log("Whoops.. Something went wrong");
        process.exit();
    }    
    console.log("\nNEW DETAILS");
    displayAllContacts([data[contactIndex]]);
    writeToDatabase(data);
};


export const editContact = async () => {
    let selectedEntry  = "";
    //let isTrue = /^[1-5q]/i.test(selectedEntry);
    while (!(/^[1-6q]/i.test(selectedEntry))) {
        selectedEntry = await askQuestion(question);
    }
    await editEntry(selectedEntry);

};