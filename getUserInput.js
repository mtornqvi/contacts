import askQuestion from "./asyncQuestion.js";
import displayAllContacts from "./displayAllContacts.js";
import {readFromDatabase, writeToDatabase} from "./connectDB.js";
import { v4 as uuidv4 } from "uuid";
import { InputValidator } from "./validator.js";

const contacts = {
    uuid: "",
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    address: "",
    notes: ""
};

// step 1: First name
export const getFirstName = async () => {
    let valid = false;
    while (!valid) {
        const firstName = await askQuestion("(Required) Insert First name: ");
        // Validate First name
        if (!firstName) {
            valid = false;
        } else {
            return firstName; //contacts.firstName
        }
    }
};

// step 2: Last name
export const getLastName = async () => {
    let valid = false;
    while (!valid) {
        const lastName = await askQuestion("(Required) Insert Family name: ");
        // Validate Last name
        if (!lastName) {
            valid = false;
        } else {
            return lastName; //contacts.lastName
        }
    }
};

// step 3: Address
export const getAddress = async () => {
    const address = await askQuestion("(Optional) Insert Address: ");
    return address; //contacts.address
};

// step 4: Phone
export const getPhoneNumber = async () => {
    let valid = false;
    while (!valid) {
        const phone = await askQuestion("(Optional) Insert Phone Number: ");
        // Validate phone number
        if (isNaN(phone)) {
            valid = false;
            console.log("Insert valid phone number e.g. +358400123123");
        } else {
            valid = true;
            return phone; //contacts.phone
        }
    } 
};

// step 5: Email
export const getEmailAddress = async () => {
    let valid = false;
    while (!valid) {
        const email = await askQuestion("(Optional) Insert Email: ");
        // Validate email
        const re = /(^$|^.*@.*\..*$)/;
        if (re.test(email)) {
            valid = true;
            return email;
        } else {
            console.log("Insert valid email.");
        }
    }
};

// step 6: Notes
export const getNotes = async () => {
    const notes = await askQuestion("(Optional) Insert Notes: ");
    // TODO : Log object and approve/discharge
    return notes;    
};

export const printSummary = (contacts) => {
    return displayAllContacts([contacts]);
};

const getUserInput = async () => { // TODO : refactor and move to separate files
    const newData = await readFromDatabase();
    contacts.uuid = uuidv4();
    contacts.firstName = await getFirstName();
    contacts.lastName = await getLastName();
    contacts.phone = await getPhoneNumber();
    contacts.email = await getEmailAddress();
    contacts.address = await getAddress();
    contacts.notes = await getNotes();
    newData.push(contacts);
    printSummary(contacts);
    const status = await InputValidator(contacts);
    if (status !== -1 ) {
        writeToDatabase(newData);
    }
};

export default getUserInput;
