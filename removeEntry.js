import askQuestion from "./asyncQuestion.js";
import {readFromDatabase, writeToDatabase} from "./connectDB.js";
import displayAllContacts from "./displayAllContacts.js";

const findContactIndex = async (database) => {
    let match = false;
    let index = "";
    while (!match) {
        let findFirstName = await askQuestion("Firstname to find: ");
        let findLastName = await askQuestion("Last name to find: ");
        findFirstName = findFirstName.toLowerCase();
        findLastName = findLastName.toLowerCase();
        
        index = await database.findIndex(x => 
            x.firstName.toLowerCase().includes(findFirstName) && 
                    x.lastName.toLowerCase().includes(findLastName)
        );
        if (index !== -1) {
            match = true;
        } else console.log("\nNo match, try again:");
    }
    
    console.log("\nFOUND");
        
    displayAllContacts([database[index]]);
    return index;
}; 

export const removeEntry = async () => {
    const data = await readFromDatabase();
    let verify = false;
    while (!verify){
        console.log("FIND CONTACT TO BE REMOVED");
        let contactIndex = await findContactIndex(data);
        verify = await askQuestion(
            `Are you sure you want to remove ${data[contactIndex].firstName} ${data[contactIndex].lastName}?\n`+
            "1. Yes 2. No\n");
        if(verify === "1"){
            data.splice(contactIndex, 1);
            verify = true;
        }
    }
    console.log("\nContact removed");
    writeToDatabase(data);
};
