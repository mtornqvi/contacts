/** **
 * Function that asks a question and returns the response
 * from stdin
 * */

import { stdin as input, stdout as output } from "process";
import readline from "readline";


const askQuestion = (question) => {
    const rl = readline.createInterface({ input, output, terminal: false });

    return new Promise((resolve) => rl.question(question, (answer) => {
        resolve(answer);
        rl.close();
    }));
};

export default askQuestion;
