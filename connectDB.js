import fs from "fs";
import util from "util";


/*** Contact details structure ***/
/* 
const contacts = {
    uuid: uuidv4(),
    firstName: "Teppo",
    lastName: "Testaaja",
    email: "teppo@testaaja.fi",
    phone: "+358400123123",
    Address: "Mannerheimintie 1, 00100 Helsinki",
} 
*/


const readFile = util.promisify(fs.readFile);
const filePath = "./database.json";


/**
 * Function to read the data from the database-file.
 * 
 *  Takes in two parameters:
 * - a callback function to modify or output the data.
 * - the name/path of the file, filePath.
 */
export const readFromDatabase =  async () => {
    const data = await readFile(filePath, "utf8"); 
    try {
        return JSON.parse(data);

    } catch (err) {
        console.log("my errorrrr:", err);
    }
};


/**
 * Function to write in the database-file. 
 * 
 * Takes in two parameters:
 * - the data to be written in the file (dataToSave)
 * - the name/path of the file, filePath.
 */
export const writeToDatabase = (dataToSave) => {
    const data = JSON.stringify(dataToSave, null, 4);
    fs.writeFile(filePath, data, "utf8", (err) => {
        if (err) console.log("Error: Could not save file.");
    } );
};
/* writeToDatabase([]);
 */

export default readFromDatabase;

